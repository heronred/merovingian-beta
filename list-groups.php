  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/style.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>SISTEMA DE GERENCIAMENTO DE MÍDIAS SOCIAIS</title>
    </head>

    <body>
	  	<div class="row">
	  		
	      <div class="col s2 sidebar-menus">
	      	<div class="hold-user-name center-align">heron g. alves</div>
	      	<div class="descricao-user-permissao center-align">Administrador</div>

	      	<nav class="menu">
	      	    <ul>
	      	     	<li><a href="#">Área de Trabalho</a></li>
	      	     	<li><a href="#">Minhas Campanhas</a></li>
	      	     	<li><a href="#" class="active">Meus Grupos</a></li>
	      	     	<li><a href="#">Clientes em Potencial</a></li>
	      	     	<li><a href="#">Clientes em Potencial</a></li>
	      	    </ul>
	      	</nav>
	        <!-- Grey navigation panel -->
	      </div>

	      <div class="col s10 ">
	      	<div class="col s12">
	      		<div class="content">
	      			<div class="hold-title-page">
	      				<h1>MEUS GRUPOS</h1>
	      				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
	      				<div class="hold-nav-menu">
	      					<ul>
	      						<li class="active"><a href="#" title="">Listar grupos</a></li>
	      						<li ><a href="#" title="">Adicionar grupos</a></li>
	      						<li ><a href="#" title="">Solicitações pendentes</a></li>
	      					</ul>
	      					<div class="clearfix"></div>
	      				</div>
	      			</div>
	      			<div class="content-sistema">
	      				<div class="row">
	      					<div class="col s12">

								<table>
							        <thead>
							          <tr>
							              <th width="10%"></th>
							              <th width="60%">Nome do GRUPO</th>
							              <th width="20%">inscritos</th>
							              <th width="10%"></th>
							          </tr>
							        </thead>

							        <tbody>
							          <tr>
							            <td><img class="responsive-img" src="https://scontent.fcgr2-1.fna.fbcdn.net/v/t1.0-0/c50.0.859.859a/s75x225/972291_1407044989382893_2696897407548531971_n.jpg?_nc_cat=106&_nc_ht=scontent.fcgr2-1.fna&oh=eec89e0eb5275b28704a58434ea039f8&oe=5D703D98"></td>
							            <td>Divas Fitness 👑💄👠</td>
							            <td>2,5 mil membros</td>
							            <td>
							            	<button class="entrar-grupo">ACESSAR</button>
							            </td>
							          </tr>
	

								          <tr>
							            <td><img class="responsive-img" src="https://scontent.fcgr2-1.fna.fbcdn.net/v/t1.0-0/c0.0.430.430a/s75x225/39293442_2263510043884955_3872928914548457472_n.jpg?_nc_cat=103&_nc_ht=scontent.fcgr2-1.fna&oh=d8afae87e49b8b7cfe2bec63ab691675&oe=5D759DD2"></td>
							            <td>Musculação, Fitness, Saúde e Bem Estar</td>
							            <td>21 mil membros</td>
							            <td>
							            	<button class="entrar-grupo">ACESSAR</button>
							            </td>
							          </tr>
							        </tbody>
							    </table>

	      					</div>
	      				</div>
	      			</div>
	      		</div>
	      	</div>
	        <!-- Teal page content  -->
	      </div>
	  	</div>
      <!--JavaScript at end of body for optimized loading-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>

    </body>
  </html>