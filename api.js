const express 	= require('express') ;
const bodyParser 	=   require('body-parser') 
const puppeteer = require('puppeteer');
const app = express() ;

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/",function(req,res){
	res.send("SEJE BEM VINDO") ;
});

app.get("/groups/:term",function(req,res){
	let term = req.params.term ;
	// console.log(term)
	// return

	puppeteer.launch().then(async browser => {
	  const page = await browser.newPage();
	  
	  await page.goto('https://www.facebook.com/groups/1817238265047480/');
	  
	  const bodyHandle = await page.$('body'); 
	  const html = await page.evaluate(body => body.innerHTML, bodyHandle); 

	  	// ini:Logar Facebook
		 	console.log('Page URL:', page.url());
		 	await page.type('#email_container', 'contato@heronge.com.br');
		 	await page.type('#pass', 'w070899');
		 	await page.click('#loginbutton');
		 	await page.waitForNavigation();
		// end:Logar Facebook

		
		await page.goto('https://www.facebook.com/search/?q='+term+'&searchtype=groups&epa=FILTERS&filters=eyJncm91cHNfc2hvd19vbmx5Ijoie1wibmFtZVwiOlwicHVibGljX2dyb3Vwc1wiLFwiYXJnc1wiOlwiXCJ9In0%3D'); 
		const htmlNew = await page.content();


			// ini:participar_auto
				let dataBtns = await page.evaluate(() => {
					var allIdsBtn = []
					var btnsContainer = document.querySelectorAll('._glk')

					btnsContainer.forEach((item, index) => { 
						var res = item.querySelector('a').getAttribute('id') ;
						document.getElementById(res).click();
						// allIdsBtn.push(res)
					})


					return allIdsBtn 

				})
				


				dataBtns.map(item => {
				  	document.querySelectorAll('._glk')
				  	console.log(item+"......");	
				})
				
				console.log(dataBtns);
				await browser.close()
				return
			// end:participar_auto


			// const allDivs = await page.$('._3u1._gli._6pe1', a => a.map(item => console.log(item)));
			
			let data = await page.evaluate(() => {
				var allData = []
				var divContainer = document.querySelectorAll('._3u1._gli._6pe1')

				divContainer.forEach((item, index) => { 
					var result = { 
						"title" : item.querySelector('._52eh._5bcu').innerText,
						"subscribers": item.querySelector('div._pac').textContent,
						"image": item.querySelector('img').getAttribute('src'),
						"link": item.querySelector('a').getAttribute('href')

					}
					allData.push(result)
				})

				return allData
			})

			console.log(data)
	  		res.send(data);

			await browser.close()

	  		return


		const hrefs = await page.$$eval('a', as => as.map(a => a.href));
	  	await bodyHandle.dispose();
	  	await browser.close();
	  	res.send(200) ; 
	});
});



//ini: my-groups


app.get("/my-groups",function(req,res){

	puppeteer.launch().then(async browser => {
	  const page = await browser.newPage();
	  
	  await page.goto('https://www.facebook.com/groups/1817238265047480/');
	  
	  const bodyHandle = await page.$('body'); 
	  const html = await page.evaluate(body => body.innerHTML, bodyHandle); 

	  	// ini:Logar Facebook
		 	// console.log('Page URL:', page.url());
		 	await page.type('#email_container', 'contato@heronge.com.br');
		 	await page.type('#pass', 'w070899');
		 	await page.click('#loginbutton');
		 	await page.waitForNavigation();
		 	console.log("OK entrei !");
		// end:Logar Facebook

		// await page.goto('https://www.facebook.com/herba.prom.7/groups?lst=100035913443063%3A100035913443063%3A1556653619'); 
		await page.goto('https://www.facebook.com/bookmarks/groups'); 
		const htmlNew = await page.content();
		console.log("OK estou na pagina de grupos ");
		
		let dataGroups = await page.evaluate(() => {
			var allGroups = []
			var groupsContainer = document.querySelectorAll('#contentArea ._3-96 li')
			// var groupsContainer = document.querySelectorAll('#GroupDiscoverCard_membership ul ._7vp') ANTIGA COM - 6

			groupsContainer.forEach((item, index) => { 

				var res = { 
					"title" : item.querySelector('.linkWrap span').innerText,
					"link": item.querySelector('._5afe').getAttribute('href')

				}
				allGroups.push(res)
			})

			return allGroups 
		})

		// page.waitForNavigation( { timeout: 0, waitUntil: 'domcontentloaded' , fullPage: true});
		// await page.setViewport({width: 1024, height: 2900});

		await page.screenshot({path: 'gruposNovo.png'});
		console.log(dataGroups)

	  	await bodyHandle.dispose();
	  	await browser.close();
	  	res.send(dataGroups);
	  	console.log("Ok ! fiz meu job e sai");
	});
});

// end:my-groups


// ini:group-statistics


app.get("/group-statistics-old/:term",function(req,res){
	let term = req.params.term ;
	console.log(term+"---")

	puppeteer.launch().then(async browser => {
	  const page = await browser.newPage();
	  
	  await page.goto('https://www.facebook.com/groups/1817238265047480/');
	  
	  const bodyHandle = await page.$('body'); 
	  const html = await page.evaluate(body => body.innerHTML, bodyHandle); 

	  	// ini:Logar Facebook
		 	console.log('Page URL:', page.url());
		 	await page.type('#email_container', 'contato@heronge.com.br');
		 	await page.type('#pass', 'w070899');
		 	await page.click('#loginbutton');
		 	await page.waitForNavigation();
		// end:Logar Facebook

		await page.goto('https://www.facebook.com/groups/'+term+'/search/?query=Promocube%20App&epa=SEARCH_BOX');
		// await page.goto('https://www.facebook.com/groups/235417500537083/search/?query=Pame&epa=SEARCH_BOX'); 
		const htmlNew = await page.content();

		
		await page.waitFor(2*5000);
		let dataPosts = await page.evaluate(() => {
			var allPosts = []
			var postsContainer = document.querySelectorAll('._4-u2._24on._3u1._4-u8')

			postsContainer.forEach((item, index) => { 

				var res = { "post": item.querySelector('._3084').getAttribute('href')}
				allPosts.push(res)
			})

			return allPosts 
		})
		await page.setViewport({width: 1024, height: 2000});
		const { height } = await bodyHandle.boundingBox();
		await bodyHandle.dispose();




		page.waitForNavigation( { timeout: 0, waitUntil: 'domcontentloaded' , fullPage: true});
		console.log(dataPosts)
		console.log('concluido com sucesso!!')
		await page.screenshot({path: 'grupos.png'});
	  	await bodyHandle.dispose();
	  	// await browser.close();
	  	// res.send(dataGroups);
	});
});


// end:group-statistics

app.get("/group-statistics/:term",function(req,res){

	let term = req.params.term ;
	puppeteer.launch().then(async browser => {

	  const page = await browser.newPage();
	  
	  await page.goto('https://www.facebook.com/groups/1817238265047480/');
	  
	  const bodyHandle = await page.$('body'); 
	  const html = await page.evaluate(body => body.innerHTML, bodyHandle); 

	  	// ini:Logar Facebook
		 	console.log('Page URL:', page.url());
		 	await page.type('#email_container', 'contato@heronge.com.br');
		 	await page.type('#pass', 'w070899');
		 	await page.click('#loginbutton');
		 	await page.waitForNavigation();
		// end:Logar Facebook

		await page.goto('https://www.facebook.com/groups/'+term+'/search/?query=Promocube%20App&epa=SEARCH_BOX');
		// await page.goto('https://www.facebook.com/groups/235417500537083/search/?query=Pame&epa=SEARCH_BOX'); 
		const htmlNew = await page.content();

	    console.log('Rolando a página ');
		// ini:scroll page
	    await page.evaluate(async () => {
	        await new Promise((resolve, reject) => {
	            try {
	                const maxScroll = Number.MAX_SAFE_INTEGER;
	                let lastScroll = 0;
	                const interval = setInterval(() => {
	                    window.scrollBy(0, 800);
	                    const scrollTop = document.documentElement.scrollTop;
	                    if (scrollTop === maxScroll || scrollTop === lastScroll) {
	                        clearInterval(interval);
	                        resolve();
	                    } else {
	                      lastScroll = scrollTop;
	                    }
	                }, 800);
	            } catch (err) {
	                console.log(err);
	                reject(err.toString());
	            }
	        });
	    });
	    // end:scroll page
	    // ini:get divs
		await page.waitFor(2*5000);
		
		// ini:group-name
			let groupName = await page.evaluate(() => {
				var name ;
				var postsContainer = document.querySelectorAll('#seo_h1_tag')
				postsContainer.forEach((item, index) => { 
					name = item.querySelector('a').innerText
				})
				return name 
			})
		// end:group-name



		// ini:img-top
			let imgGroup = await page.evaluate(() => {
				var imgGroupLink ;
				var postsContainer = document.querySelectorAll('._4on8')
				postsContainer.forEach((item, index) => { 
					imgGroupLink = item.querySelector('img').getAttribute('src')
				})
				return imgGroupLink 
			})
		// end:img-top





		// ini:inscritos
			let qntInscritos = await page.evaluate(() => {
				var qntd ;
				var qntInscContainer = document.querySelectorAll('#groupsNewMembersLink')
				qntInscContainer.forEach((item, index) => { 
					qntd = item.querySelector('#count_text').innerText
				})
				return qntd 
			})
		// end:inscritos

		// ini:numeros-gerais
			let dataPosts = await page.evaluate(() => {
				var allPosts = []
				var postsContainer = document.querySelectorAll('._4-u2._24on._3u1._4-u8')

				postsContainer.forEach((item, index) => { 

					// var res = {
					// 	"post":   item.querySelector('._3084').getAttribute('href'),
					// 	"likes":  item.querySelector('._3dlh').innerText,
					// 	"shares": item.querySelector('._78k7').innerText

					// }
						var res = {
						"post":   (item.querySelector('._3084')) ? item.querySelector('._3084').getAttribute('href') : "",
						"likes":  (item.querySelector('._3dlh')) ? item.querySelector('._3dlh').innerText  : "0 ",
						"shares": (item.querySelector('._78k7')) ? item.querySelector('._78k7').innerText : "0 "
						}
					allPosts.push(res)
				})

				return allPosts 
			})
		// end:numeros-gerais
		page.waitForNavigation( { timeout: 0, waitUntil: 'domcontentloaded' , fullPage: true});
		// end:get divs
		if(imgGroup == undefined){
			imgGroup = "https://static.xx.fbcdn.net/rsrc.php/v3/yj/r/Ls6GKw3fOr6.png";
		}
		console.log(imgGroup)




		await page.setViewport({width: 1024, height: 2900});
		await page.screenshot({path: 'grupos.png'});
	    console.log('Finished scrolling');
	    res.send({"name": groupName, "posts":dataPosts,"img":imgGroup, "inscritos":qntInscritos});
	    await browser.close();
	})
})



app.post('/postar',function(req,res){


	let term = req.params.term ;
	// const content_search = '#BrowseResultsContainer';
	puppeteer.launch().then(async browser => {



		var msg = req.body.msg ;
		console.log(msg); 

	  const page = await browser.newPage();
	  
	  await page.goto('https://www.facebook.com/groups/1817238265047480/');
	  
	  const bodyHandle = await page.$('body'); 
	  const html = await page.evaluate(body => body.innerHTML, bodyHandle); 

	  	// ini:Logar Facebook
		 	// console.log('Page URL:', page.url());
		 	await page.type('#email_container', 'contato@heronge.com.br');
		 	await page.type('#pass', 'w070899');
		 	await page.click('#loginbutton');
		 	await page.waitForNavigation();
		 	console.log("OK ! estou dentro ");
		// end:Logar Facebook


			await page.goto('https://www.facebook.com/bookmarks/groups'); 


  			const TEXT_POST_FIELD_CLICK = '._4h97 textarea';
  			const TEXT_POST_WRITE = '._1p1t';
  			const CLICK_POST = 'button._1mf7';


			let dataGroups = await page.evaluate(() => {
				var allGroups = []
				var divGroups = document.querySelectorAll('#contentArea ._3-96 li')

				divGroups.forEach((item, index) => { 
					var resGp = "https://www.facebook.com"+item.querySelector('._5afe').getAttribute('href') ;
					allGroups.push(resGp)
				})
				return allGroups 
			})

			console.log(dataGroups)
			
			



			await page.setViewport({width: 1024, height: 2900});
			page.waitForNavigation( { timeout: 0, waitUntil: 'domcontentloaded' , fullPage: true});
			
			for (let i = 0; i < dataGroups.length; i ++) {
			  	
			  	await page.goto(dataGroups[i], { waitUntil: "load" });
			  	await page.waitFor(2000);


				let dataDiv = await page.evaluate(() => {
					var divGroups = document.querySelectorAll('._4h97 textarea')
					var retorna = []
					divGroups.forEach((item, index) => { 
						var resGp = "GROUP :" ;
						retorna.push(resGp)
					})
					return retorna 
				})


				let groupVenda = await page.evaluate(() => {
					var divGroupsVenda = document.querySelectorAll('.fbReactComposerAttachmentSelector_STATUS ._5qtp')
					var retornaVenda = []
					divGroupsVenda.forEach((item, index) => { 
						var resGpVenda = "GROUP :" ;
						retornaVenda.push(resGpVenda)
					})
					return retornaVenda 
				})


				// Checa os grupos de post apenas
				if(dataDiv.length > 0){

				  	// ini:publicando
						await page.click(TEXT_POST_FIELD_CLICK);
						await page.waitFor(2*1000);
						await page.type(TEXT_POST_WRITE, msg);
						await page.waitFor(8000);
						await page.click(CLICK_POST);
				  		await page.screenshot({path: 'grupos'+i+'.png'});
				  		// await page.waitFor(1000);
					// end:publicando				
					console.log("ABERTO :"+i)



				}else if(groupVenda.length > 0){

				  	// ini:publicando
						// await page.click('.fbReactComposerAttachmentSelector_STATUS ._5qtp');
						// await page.waitFor(2*1000);
						// await page.type('._1p1t', msg);
						// await page.waitFor(8000);
						// await page.click(CLICK_POST);
				  // 		await page.screenshot({path: 'grupos'+i+'.png'});
				  // 		await page.waitFor(1000);
					// end:publicando	
						
						console.log("FECHADO :"+i) 
				}
				// console.log(dataDiv)
				await page.waitFor(2*1000);
				page.waitForNavigation( { timeout: 0, waitUntil: 'domcontentloaded' , fullPage: true});
			  	
			}
			
			await page.goto('https://www.facebook.com/groups/');
			await page.screenshot({path: 'final.png'});
			console.log("Concluido fofo");
			// await browser.close();
			

	})

	res.send(200);

	
})




app.post('/postar-feed',function(req,res){


	let term = req.params.term ;
	puppeteer.launch().then(async browser => {

		var msg = req.body.msg ;
		console.log(msg); 

	  const page = await browser.newPage();
	  
	  await page.goto('https://www.facebook.com/groups/1817238265047480/');
	  
	  const bodyHandle = await page.$('body'); 
	  const html = await page.evaluate(body => body.innerHTML, bodyHandle); 

	  	// ini:Logar Facebook
		 	// console.log('Page URL:', page.url());
		 	await page.type('#email_container', 'contato@heronge.com.br');
		 	await page.type('#pass', 'w070899');
		 	await page.click('#loginbutton');
		 	await page.waitForNavigation();
		 	console.log("OK ! estou dentro ");
		// end:Logar Facebook

		await page.goto('https://www.facebook.com/');

		page.waitForNavigation( { timeout: 0, waitUntil: 'domcontentloaded' , fullPage: true});
	    console.log('Rolando a página ');
		// ini:scroll page
	    await page.evaluate(async () => {
	        await new Promise((resolve, reject) => {
	            try {
	                const maxScroll = Number.MAX_SAFE_INTEGER;
	                let lastScroll = 0;
	                const interval = setInterval(() => {
	                    window.scrollBy(0, 8000);
	                    const scrollTop = document.documentElement.scrollTop;
	                    if (scrollTop === maxScroll || scrollTop === lastScroll) {
	                        clearInterval(interval);
	                        resolve();
	                    } else {
	                      lastScroll = scrollTop;
	                    }
	                }, 8000);
	            } catch (err) {
	                console.log(err);
	                reject(err.toString());
	            }
	        });
	    });
	    await page.waitFor(2*3000);
	    page.waitForNavigation( { timeout: 0, waitUntil: 'domcontentloaded' , fullPage: true});
	    // end:scroll page

		// document.querySelectorAll('._4-u2.mbm._4mrt._5v3q._7cqq._4-u8')
		// #u_fetchstream_1_0 ._5jmm._5pat._3lb4.f_lucw9w-fv


		let allPostsFeed = await page.evaluate(() => {
			// var divsPostsFeed = document.querySelectorAll('#u_fetchstream_1_0 ._7c_r._4w79')
			var divsPostsFeed = document.querySelectorAll('#u_fetchstream_1_0 ._5jmm._5pat._3lb4.f_lucw9w-fv')
			var retornaPostsFeed = []
			divsPostsFeed.forEach((item, index) => { 

				var res = item.querySelector('._4-u2.mbm._4mrt._5v3q._7cqq._4-u8').getAttribute('id')
				
				retornaPostsFeed.push(res)
			})
			return retornaPostsFeed 
		})

		await page.setViewport({width: 1024, height: 5600});
		for (let i = 0; i < allPostsFeed.length; i ++) {
			// ._1mf._1mj
			await page.click('#'+allPostsFeed[i]+" ._4w79");
			await page.waitFor(2*3000);
			await page.type('#'+allPostsFeed[i]+' ._1mf._1mj', msg);
			// await page.keyboard.down('Enter');
			// await page.keyboard.up('Enter');
			await page.waitFor(2*4000);
			// _1mf _1mj
			
			console.log("meu Feed : "+allPostsFeed[i]) ;
			await page.screenshot({path: 'feed'+i+'.png'});
			return
		}


		console.log(allPostsFeed);
		// await page.screenshot({path: 'feedTexto.png'});

		console.log("Concluido fofo");

	})

	res.send(200);

	
})




app.listen(5000,function(){
	console.log('Executando PACIFICAMENTE \n -------- --- - - -- - -- - - -- - - -- - -- - - -- - -- -');
});

